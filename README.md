# codegen-cli

CLI programs for swagger-codegen and swagger-js-codegen.


Currently, the repo includes one module `jscli`, which encapsulates the [wcandillon/swagger-js-codegen](https://github.com/wcandillon/swagger-js-codegen) library and exposes its basic functions as CLI. Note that swagger-js-codegen supports generating server stub code in javascript and typescript, and it supports only the json format for Swagger definition/spec files.

_Note: swagger-api/swagger-codegen support is still tbd._
