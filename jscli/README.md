# codegen-cli jscli

CLI program for wcandillon/swagger-js-codegen.

You can use this tool to generate server stub code in typescript and javascript (node.js) using Swagger definition files (json only).


_Work in progress_


## Overview


To install the cli/binary, run:

```bash
$ npm i -g @openapis/codegen-cli-js
```

For usage information, try:

```bash
$ codegen-js --help
```


## License

MIT © [Harry Y](mailto:sidewaybot@yahoo.com)
