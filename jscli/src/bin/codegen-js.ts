#!/usr/bin/env node

import * as fs from 'fs';
import * as path from 'path';
import * as program from 'commander';
var CodeGen: any = require('swagger-js-codegen').CodeGen;   // ???
import { UtilCamelCase } from '../util/camel-case';


export class CodegenJs {

  // CLI flags.
  private _isTS: boolean;
  private _swaggerFiles: string[];
  // tbd: output file folder?

  constructor() {
    this._isTS = false;
    this._swaggerFiles = [];
  }

  private _parseArgs() {
    program
      .version('0.1.2')    // tbd: Keep this in sync with package version ??? --> Is there a better way?
      .usage('[options] <swagger json file ...>')
      .option('-t, --typescript', 'Outputs typescript code')
      // .option('-c, --classname <class>', 'Generated class name')
      .parse(process.argv);

    if (program.typescript) {
      this._isTS = true;
    }
    // console.log('> _isTS = ' + this._isTS);

    this._swaggerFiles = program.args.slice(0);
    // console.log('> args: %j', this._swaggerFiles);
  }

  private _processSwaggerFiles() {
    for (let f of this._swaggerFiles) {
      // TBD: Use async version. (Just testing for now...)
      let swagger = JSON.parse(fs.readFileSync(f, 'UTF-8'));

      let dirname = path.dirname(f);
      let basename = path.basename(f, '.json');
      // console.log('> basename = ' + basename);
      let className = UtilCamelCase.camelCase(basename);
      // console.log('> className = ' + className);
      let ext = ".js";
      if (this._isTS) {
        ext = ".ts";
      }
      let codefile = dirname + path.sep + basename + ext;
      // console.log('> codefile = ' + codefile);

      // temporary
      let code: string;
      if (this._isTS) {
        // ??? What is imports?
        // code = CodeGen.getTypescriptCode({ className: className, swagger: swagger, imports: ['../../typings/tsd.d.ts'] });
        code = CodeGen.getTypescriptCode({ className: className, swagger: swagger });
      } else {
        code = CodeGen.getNodeCode({ className: className, swagger: swagger });
      }
      // console.log('> code = ' + code);

      // TBD: Use async version.
      fs.writeFileSync(codefile, code, 'UTF-8');

      // Feedback to the user.
      process.stdout.write('Generated: ' + codefile);
    }
  }

  public static main() {
    let codegen = new CodegenJs();
    codegen._parseArgs();
    codegen._processSwaggerFiles();
    // ...
  }
}

CodegenJs.main();
